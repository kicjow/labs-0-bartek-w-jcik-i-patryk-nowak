﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0.Main
{
    class Liczba
    {
        public Liczba(int liczba)
        {
            this.liczba = liczba;
        }
        public Liczba()
        {
            this.liczba = 0;
        }
                
        
        public int liczba { get; set; }
    
        public virtual int Wyswietl()
        {
            Console.WriteLine("To jest liczba");
            return liczba;
        }
    }


    class Liczba_Parzysta : Liczba
    {
        public Liczba_Parzysta(int liczba)
            : base(liczba)
        {
        }
        public Liczba_Parzysta()
        {
            this.liczba = 2;
        }

        public override int Wyswietl()
        {
            Console.WriteLine("To jest liczba parzysta");
            return this.liczba + 1;
        }
    }


    class Liczba_Nieparzysta : Liczba
    {
        public Liczba_Nieparzysta(int liczba)
            : base(liczba)
        {
          
        }
         public Liczba_Nieparzysta()
        {
            this.liczba = 3;
        }
        public override int Wyswietl()
        {
            Console.WriteLine("To jest liczba nieparzysta");
            return this.liczba + 2;
        }






















































    }
    class Program
    {
        static void Main(string[] args)
        {
            Liczba zero = new Liczba(0);
           Console.WriteLine( zero.Wyswietl());
            Liczba_Parzysta dwa = new Liczba_Parzysta(2);
             Console.WriteLine( dwa.Wyswietl());

            Liczba_Nieparzysta trzy = new Liczba_Nieparzysta(3);;
            Console.WriteLine(  trzy.Wyswietl());

            Console.ReadKey();
        }
    }
}
