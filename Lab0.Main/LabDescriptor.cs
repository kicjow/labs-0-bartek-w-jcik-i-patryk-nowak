﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0.Main
{
    public class LabDescriptor
    {
        public static Type A = typeof(Liczba);
        public static Type B = typeof(Liczba_Parzysta);
        public static Type C = typeof(Liczba_Nieparzysta);

        public static string commonMethodName = "Wyswietl";
    }
}
